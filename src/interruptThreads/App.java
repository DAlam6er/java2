package interruptThreads;

public class App
{
    public static void main(String[] args)
    {
        StartStopThread thread = new StartStopThread();

        System.out.println("start: " + thread.start());
        System.out.println("join: " + thread.join(2000)); // 2 с
        /*
        long x = thread.interrupt(); //wait() wait(ms) sleep(ms) join() ...
        System.out.println("interrupt: " + x);
         */
        System.out.println("stop: " + thread.stop());
        System.out.println("end of main()");
    }
}
