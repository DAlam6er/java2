package interruptThreads;

public class StartStopThread implements Runnable
{
    private Thread self;
    private long counter;
    private volatile boolean canWork; // volatile запрещает оптимизацию по данной переменной

    public StartStopThread()
    {
        self = new Thread(this);
        counter = 0;
    }

    public long start()
    {
        canWork = true;
        self.start();
        return counter;
    }

    public long stop()
    {
        canWork = false;
        return counter;
    }

    public long interrupt()
    {
        System.out.println("Sending interrupt");
        self.interrupt(); // будет работать только с wait, sleep, join
        return counter;
    }

    public long join(int ms)
    {
        try {
            self.join(ms);
        } catch (InterruptedException e) {
            return -1; // never get here!
        }
        return counter;
    }

    @Override
    public void run()
    {
      /*
      try {
            while (true) {
                counter++;
                Thread.sleep(100); // тайм-аут 100 мс
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted!");
        }
       */
        while(canWork) {
            counter++;
        }
    }
}
