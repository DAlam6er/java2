package threadsPool;

import java.util.ArrayList;
import java.util.concurrent.*;

public class App
{
    public static void main(String[] args)
        throws ExecutionException, InterruptedException
    {
        ArrayList<Future<String>> results = new ArrayList<>();
        // класс-утилита Executors (множ. число), исполнителей 5
//        ExecutorService exec = Executors.newFixedThreadPool(5);
        // будет создано столько потоков, сколько ядер у процессора
        ExecutorService exec = Executors.newWorkStealingPool();
        for (int i = 0; i < 12; i++) { // 12 задач
            //exec.submit(new Callable<String>()
            // добавляем ссылки на ЕЩЁ не проинициализированные объекты
            results.add(exec.submit(new Callable<String>()
            {
                @Override
                public String call() throws Exception
                {
                    // программа будет работать 3 секунды (5+5+2)
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() +
                        " - working");
                    return Thread.currentThread().getName();
                }
            }));
        }
        exec.shutdown();
        for (Future<String> result : results) {
            System.out.println(result.get() + " - sent result");
        }
        System.out.println("end of main()");
    }
}
