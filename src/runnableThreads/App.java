package runnableThreads;

public class App
{
    public static void main(String[] args) throws InterruptedException
    {
        DemoThread thread = new DemoThread();
        for (int i = 0; i < 10; i++) {
            System.out.println("main() - " + i);
            Thread.sleep(100); // не гарантирует синхронизации так, что потоки будут выполняться один за другим, реальная задержка 100,000005 (например)
        }
        thread.join();

        System.out.println("End of main()");
    }
}
