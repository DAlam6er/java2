package runnableThreads;

public class DemoThread implements Runnable
{
    private Thread self; // собственный поток

    public DemoThread()
    {
        self = new Thread(this,"RunnableDemo");
        self.start();
    }

    public void join()
    {
        try {
            self.join();
        } catch (InterruptedException e) {
            // ...
        }
    }

    @Override
    public void run()
    {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + " - " + i);
            try {
                Thread.sleep(100); // без него никогда не получим Interrupted, т.е. цикл всегда отработает 10 раз
            } catch (InterruptedException e) {
                break;
            }
        }

    }
}
