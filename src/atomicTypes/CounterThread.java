package atomicTypes;

public class CounterThread implements Runnable
{
    private Thread self = new Thread(this); // не слишком распространенная практика, лучше в конструкторе

    public void start()
    {
        self.start();
    }

    public void join()
    {
        try {
            self.join();
        } catch (InterruptedException e) {
        }
    }

    @Override
    public void run()
    {
        int i;
        for (i = 0; i < GlobalData.STEPS; i++) {
            // синхронизация в цикле замедляет весь цикл!
            // синхронизация по методу
            GlobalData.value++;
            GlobalData.aValue.getAndIncrement();
        }
        System.out.println("i = " + i);
    }
}
