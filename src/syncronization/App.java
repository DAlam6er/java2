package syncronization;

import javax.management.monitor.MonitorNotification;

public class App
{
    static int x = 0;
    public static void main(String[] args) throws InterruptedException
    {
        Test test = new Test();
        final int steps = 1000;
        final int thNumber = 5; // number of threads

        Runnable runner1 = new Runnable()
        {
            @Override
            public void run()
            {
                for (int i = 0; i < steps; i++) {
                    test.increment();
                }
            }
        };

//        Monitor mon = new Monitor();
        Object mon = new Object();

        Runnable runner2 = new Runnable()
        {
            @Override
            public void run()
            {
                for (int i = 0; i < steps; i++) {
                    synchronized (mon) {
//                        mon.x++;
                        x++;
                        // любой код между { и } будет синхронизирован
                    }
                }
            }
        };

        Thread[] threads = new Thread[thNumber]; // for each - нельзя, только для чтения
        for (int i = 0; i < thNumber; i++) { // создаем и стартуем все потоки
//            threads[i] = new Thread(runner1);
            threads[i] = new Thread(runner2);
            threads[i].start();

        }
        for (int i = 0; i < thNumber; i++) { // join в другом цикле, т.к. join блокирующий вызов
            threads[i].join();
        }

//        System.out.println("test.x = " + test.getX());
//        System.out.println("monitor.x = " + mon.x);
        System.out.println("monitor.x = " + x);
    }
}

class Test
{
    private int x;

    public int getX()
    {
        return x;
    }

    // синхронизация по методу, х гарантированно инкрементировался в каждом потоке
    public synchronized void increment()
    {
        x++;
    }
}

class Monitor
{
    int x; // x виден только внутри package
}
