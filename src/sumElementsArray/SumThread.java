package sumElementsArray;

public class SumThread extends Thread
{
    private final int position;

    public SumThread(int position)
    {
        this.position = position;
    }

    @Override
    public void run()
    {
        for (int i = position; i < GlobalData.ARRAY_SIZE;
            i += GlobalData.THREADS_COUNT)
        {
            GlobalData.results[position] += GlobalData.array[i];
        }
    }
}
