package sumElementsArray;

public class GlobalData
{
    public static final int ARRAY_SIZE = 1000000000; // 1*10^9
    public static final int THREADS_COUNT = 2;

    public static int[] array;
    public static long[] results; // массив ответов
}
