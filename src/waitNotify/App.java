package waitNotify;

public class App
{
    public static void main(String[] args) throws InterruptedException
    {
        Monitor mon = new Monitor();
        Thread th1 = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                for (int i = 0; i < 100; i++) {
                    System.out.println("1 - " + i);
                    if (i >= 50) {
                        synchronized (mon) {
                            mon.x = i;
                            mon.notify();
                        }
                    }
                }
            }
        });
        Thread th2 = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                synchronized (mon) {
                    // одна из проверок избыточна - if или while
                    // зато надёжно, учитывая все случаи с 1-м потоком
                    while (mon.x < 50) { // while гарантирует, что мы не проснемся, до того как условие пройдет, чтобы вернуться в сон, если разбудили раньше времени
                        try {
                            mon.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                }
                for (int i = 0; i < 100; i++) {
                    System.out.println("2 - " + i);
                }
            }
        });

        th1.start();
        Thread.sleep(2);
        th2.start();

        th1.join();
        th2.join();
    }
}

class Monitor
{
    public volatile int x;
}
