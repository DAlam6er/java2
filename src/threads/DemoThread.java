package threads;

public class DemoThread extends Thread
{
    @Override
    public void run()
    {
        System.out.println("Hello from " + Thread.currentThread().getName());
    }
}
